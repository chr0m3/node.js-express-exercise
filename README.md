# Node.js Express Exercise

Node.js와 Express Framework를 사용해 REST API 서버를 개발해봅니다.

## References

* [https://nodejs.org/](https://nodejs.org/)
* [https://expressjs.com/](https://expressjs.com/)
* [https://sequelize.org/](https://sequelize.org/)
* [https://www.restapitutorial.com/](https://www.restapitutorial.com/)

## Instruction

### 1. Installation

Node.js와 npm이 설치되어 있는 상태에서 시작합니다.

#### 1. Create Express Project with express-generator

```bash
$ npx express-generator --no-view
```

##### Install Dependencies

```bash
$ npm install
```

#### 2. Install Sequelize.js

```bash
$ npm install --save sequelize
```

##### Install Database Connector Library

###### PostgreSQL

* `pg@^7.0.0`
* `pg-hstore`

```bash
$ npm install --save pg pg-hstore
```

###### MySQL

* `mysql2@^1.5.2`

```bash
$ npm install --save mysql2
```

###### SQLite

* `sqlite3@^4.0.0`

```bash
$ npm install --save sqlite3
```

#### 3. Install ESLint

```bash
$ npm install --save-dev eslint
$ eslint --init
```

* To check syntax, find problems, and enforce code style
* CommonJS (require/exports)
* None of these
* No
* Node
* Use a popular style guide
* Airbnb: https://github.com/airbnb/javascript
* JSON
* Yes

##### Add Command

```json
// package.json

"scripts": {
  ...
  "lint": "eslint ."
},
```

#### 4. Install nodemon

```bash
$ npm install --save-dev nodemon
```

##### Add Command

```json
// package.json

"scripts": {
  ...
  "nodemon": "nodemon ./bin/www"
},
```

### 2. Make Configurations

#### 1. Make Config Module

`/config` 디렉터리와 `/config/index.js` 파일을 생성합니다.
`index.js` 파일은 `/config` 디렉터리 내에 있는 파일을 import하여 dictionary로 export 해줍니다.
결과적으로 데이터베이스 설정값을 가지고 있는 `/config/db.js` 파일이 있다면 `const { db } = require('./config');`와 같이 설정값에 접근할 수 있게 됩니다.

#### 2. Make Database Configurations

데이터베이스 설정값을 가지고 있는 `/config/db.js` 파일을 생성합니다.
로컬에서 개발할 때와 테스트할 때, 그리고 production에서의 설정값이 다르므로 각 설정값 파일은 상황별 설정값을 dictionary로 export해야 합니다.
따라서 export되는 dictionary는 아래와 같은 형태이면 됩니다.

```text
{
  'development': {
    'key1': 'value',
    'key2': 'value',
    ...
  },
  'test': {
    'key1': 'value',
    'key2': 'value',
    ...
  },
  'production': {
    'key1': 'value',
    'key2': 'value',
    ...
  },
  ...
}
```

### 3. Make Models

#### 1. Make Models Module

`/models` 디렉터리와 `/models/index.js` 파일을 생성합니다.
`/models/index.js` 파일의 내용은 아래와 같습니다.

```js
const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');

const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';
// eslint-disable-next-line import/no-dynamic-require
const config = require(`${__dirname}/../config`).db[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter((file) => (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js'))
  .forEach((file) => {
    const model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

db.MyModel = require('./mymodel')(sequelize, Sequelize);
]
Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

module.exports = db;
```

우리가 정의한 model들을 데이터베이스에 반영시키기 위해 `/app.js` 파일을 변경합니다.
`app.js`가 `Sequelize` instance의 `sync()` method를 호출하면 웹 서버가 구동될 때 정의된 model을 데이터베이스에 반영합니다.
`Sequelize` class는 `/models/index.js` 파일에서 instanciate되어 export됩니다(위 코드 참고).
따라서 `const { sequelize } = require('./models/index');`와 같이 `models` 모듈을 import하여 호출할 수 있습니다.

#### 2. Make Models

##### Model Definition

```js
/* mymodel.js */
module.exports = (sequelize, DataTypes) => {
  const MyModel = sequelize.define('mymodel', {
    id: {
      allowNull: false,
      primaryKey: true,
      type: DataTypes.INTEGER,
    },
    name: {
      allowNull: false,
      type: DataTypes.STRING(64),
    },
  }, {
    timestamps: true,
    underscored: true,
  });

  MyModel.associate = (models) => {
    MyModel.belongsTo(models.anothermodel, {
      foreignKey: {
        allowNull: false,
      },
    });
  };

  return MyModel;
};
```

##### Team

* `id`
  * Integer
  * Primary Key
  * Auto Increment
* `name`
  * Varchar(32)
  * Not Null

##### Employee

* `id`
  * Integer
  * Primary Key
  * Auto Increment
* `name`
  * Varchar(32)
  * Not Null
* `salary`
  * Integer
  * Not Null
* `employee`
  * Integer
  * Foreign Key(employee)
  * Not Null

### 3. Make Routes

#### 1. Make Route Files

사용자의 요청을 처리할 route들은 `/routes` 디렉터리 내에 정의됩니다.
express-generator를 사용하여 프로젝트를 생성하면 기본적으로 `/routes/index.js` 파일이 생성되어 있습니다.
이 파일을 복사하여 `/routes/employee.js`와 `/routes/employee.js` 파일을 만듭니다.

#### 2. Implement Router

`/routes/team.js` 파일과 `/routes/employee.js` 파일에 아래 요구사항에 맞게 각각의 API를 구현합니다.

##### Sample Router

```js
/* myrouter.js */
const express = require('express');
const db = require('../models');

const router = express.Router();

const { User } = db;

router.get('/', (req, res, next) => {
  User.findAll()
    .then((users) => {
      const res = {
        users,
      };

      res
        .status(200)
        .json(res);

      return next();
    })
    .catch((e) => {
      const res = {
        message: e.message,
      };

      res
        .status(500)
        .json(res);

      return next();
    });
});
```

##### Team API

###### List Team

|**METHOD**|**ENDPOINT**|
|-|-|
|GET|`/api/team`|

* 전체 employee 목록을 반환합니다.

###### Create Team

|**METHOD**|**ENDPOINT**|
|-|-|
|POST|`/api/team`|

* 새로운 team을 생성합니다.

```json
{
  "name": STRING
}
```

* 성공한 경우 `201 Created` 상태를 반환합니다.
* 성공한 경우 생성된 team을 반환합니다.

```json
{
  "employee": OBJECT(employee)
}
```

* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

###### Read Team

|**METHOD**|**ENDPOINT**|
|-|-|
|GET|`/api/team/:id`|

<table>
  <tr>
    <th colspan=3>REQUEST PARAMETER</th>
  </tr>
  <tr>
    <td>
      <code>id</code>
    </td>
    <td>
      Integer
    </td>
    <td>
      Team id
    </td>
  </tr>
</table>

* 요청한 team 정보를 읽습니다.


* 성공한 경우 `200 OK` 상태를 반환합니다.
* 성공한 경우 요청한 team을 반환합니다.

```json
{
  "team": OBJECT(TEAM)
}
```

* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 존재하지 않는 team을 요청했다면 `404 Not Found` 상태를 반환합니다.
* 존재하지 않는 team을 요청했다면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

###### Update Team

|**METHOD**|**ENDPOINT**|
|-|-|
|PUT|`/api/team/:id`|

<table>
  <tr>
    <th colspan=3>REQUEST PARAMETER</th>
  </tr>
  <tr>
    <td>
      <code>id</code>
    </td>
    <td>
      Integer
    </td>
    <td>
      Team id
    </td>
  </tr>
</table>

* 해당 team의 정보를 주어진 정보로 교체합니다.


* 성공한 경우 `200 OK` 상태를 반환합니다.
* 성공한 경우 변경된 team을 반환합니다.

```json
{
  "employee": OBJECT(TEAN)
}
```

* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 존재하지 않는 team을 변경하려고 시도했다면 `404 Not Found` 상태를 반환합니다.
* 존재하지 않는 team을 변경하려고 시도했다면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

###### Delete Team

|**METHOD**|**ENDPOINT**|
|-|-|
|PUT|`/api/team/:id`|

<table>
  <tr>
    <th colspan=3>REQUEST PARAMETER</th>
  </tr>
  <tr>
    <td>
      <code>id</code>
    </td>
    <td>
      Integer
    </td>
    <td>
      Team id
    </td>
  </tr>
</table>

* 해당 team을 삭제합니다.


* 성공한 경우 `204 No Content` 상태를 반환합니다.


* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 존재하지 않는 team을 삭제하려고 시도했다면 `404 Not Found` 상태를 반환합니다.
* 존재하지 않는 team을 삭제하려고 시도했다면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

##### Employee API

###### List Employee

|**METHOD**|**ENDPOINT**|
|-|-|
|GET|`/api/employee`|

* 전체 employee 목록을 반환합니다.

###### Create Employee

|**METHOD**|**ENDPOINT**|
|-|-|
|POST|`/api/employee`|

* 새로운 employee를 생성합니다.

```json
{
  "name": STRING
}
```

* 성공한 경우 `201 Created` 상태를 반환합니다.
* 성공한 경우 생성된 employee를 반환합니다.

```json
{
  "employee": OBJECT(EMPLOYEE)
}
```

* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

###### Read Employee

|**METHOD**|**ENDPOINT**|
|-|-|
|GET|`/api/employee/:id`|

<table>
  <tr>
    <th colspan=3>REQUEST PARAMETER</th>
  </tr>
  <tr>
    <td>
      <code>id</code>
    </td>
    <td>
      Integer
    </td>
    <td>
      Employee id
    </td>
  </tr>
</table>

* 요청한 employee 정보를 읽습니다.


* 성공한 경우 `200 OK` 상태를 반환합니다.
* 성공한 경우 요청한 employee를 반환합니다.

```json
{
  "employee": OBJECT(EMPLOYEE)
}
```

* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 존재하지 않는 employee를 요청했다면 `404 Not Found` 상태를 반환합니다.
* 존재하지 않는 employee를 요청했다면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

###### Update Employee

|**METHOD**|**ENDPOINT**|
|-|-|
|PUT|`/api/employee/:id`|

<table>
  <tr>
    <th colspan=3>REQUEST PARAMETER</th>
  </tr>
  <tr>
    <td>
      <code>id</code>
    </td>
    <td>
      Integer
    </td>
    <td>
      employee id
    </td>
  </tr>
</table>

* 해당 employee의 정보를 주어진 정보로 교체합니다.


* 성공한 경우 `200 OK` 상태를 반환합니다.
* 성공한 경우 변경된 employee를 반환합니다.

```json
{
  "employee": OBJECT(EMPLOYEE)
}
```

* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 존재하지 않는 employee를 변경하려고 시도했다면 `404 Not Found` 상태를 반환합니다.
* 존재하지 않는 employee를 변경하려고 시도했다면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

###### Delete Employee

|**METHOD**|**ENDPOINT**|
|-|-|
|PUT|`/api/employee/:id`|

<table>
  <tr>
    <th colspan=3>REQUEST PARAMETER</th>
  </tr>
  <tr>
    <td>
      <code>id</code>
    </td>
    <td>
      Integer
    </td>
    <td>
      Employee id
    </td>
  </tr>
</table>

* 해당 employee를 삭제합니다.


* 성공한 경우 `204 No Content` 상태를 반환합니다.


* 요청이 올바르지 않으면 `400 Bad Request` 상태를 반환합니다.
* 요청이 올바르지 않으면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 존재하지 않는 employee를 삭제하려고 시도했다면 `404 Not Found` 상태를 반환합니다.
* 존재하지 않는 employee를 삭제하려고 시도했다면 적절한 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

* 내부 에러가 발생했다면 `500 Internal Error` 상태를 반환합니다.
* 내부 에러가 발생했다면 에러 메세지를 반환합니다.

```json
{
  "message": STRING
}
```

#### 3. Use Routers

만든 router를 사용하기 위해 `/app.js` 파일을 변경합니다.
`app.use(route, routerInstance)`를 호출하여 router를 사용할 수 있습니다.
Router instance는 각 routes 모듈의 각 파일이 export 하므로 사용하고자 하는 router를 import하면 됩니다.
따라서 `const myRouter = require('./routes/my');`와 같이 import하고, `app.use('/my', myRouter);`와 같이 호출합니다.
